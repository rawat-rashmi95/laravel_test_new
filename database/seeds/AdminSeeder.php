<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = array( array('name' => 'Admin',
    						  'email' => 'test@gmail.com',
    						  'password' => Hash::make('test@gmail.com'),
    						  'created_at' => date('Y-m-d H:i:s'),
    					),
						array('name' => 'Admin',
    						  'email' => 'test1@gmail.com',
    						  'password' => Hash::make('test1@gmail.com'),
    						  'created_at' => date('Y-m-d H:i:s'),
    					)
    			);
		foreach($array as $key => $value){
			$get_user_by_email = User::where('email',$value['email'])->first();
			if(empty($get_user_by_email)){
				User::insert($value);
			} 
		}
    }
}
