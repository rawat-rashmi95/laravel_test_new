<?php
if (! function_exists('pre')) {
    function pre($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

if (! function_exists('healthIssues')) {
    function healthIssues() {
        $data = [
        		'Health Issues 1',
        		'Health Issues 2',
        		'Health Issues 3',
        		'Health Issues 4',
        		'Health Issues 5'
        		
        ];
		return $data;
    }
}
if (! function_exists('bookingType')) {
    function bookingType() {
        $data = [
                'Indigo',
                'Spice Jet',
        ];
        return $data;
    }
}
if (! function_exists('extraCharges')) {
    function extraCharges($price,$days) {
        $main_price = 0;
        if($days > 45){
            $main_price = $price;
        }elseif ($days > 30 && $days <= 45) {
           $main_price = ($price * 30) / 100;
        }elseif ($days > 15 && $days <= 30) {
           $main_price = ($price * 50) / 100;
        }elseif ($days > 5 && $days <= 15) {
           $main_price = ($price * 80) / 100;
        }elseif($days){
           $main_price = $price * 2;
        }
        return $main_price;
    }
}
if (! function_exists('getBirthdayDiscount')) {
    function getBirthdayDiscount($price) {
       
        $main_price = ($price * 10) / 100;
        $main_price = $price - $main_price;
        return $main_price;
    }
}