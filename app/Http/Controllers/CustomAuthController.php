<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Admin;
use App\UserQueryModel;
use App\FlightBookingQueryModel;
use Illuminate\Support\Facades\Auth;

class CustomAuthController extends Controller
{
    public function index()
    {
        return view('login');
    }  
    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->intended('dashboard')
                        ->withSuccess('Signed in');
        }
  
        return redirect('/')->with('error', 'Login details are not valid');
    }
    public function dashboard()
    {
        if(Auth::check()){
            $title = 'Dashboard';
            return view('myHome',compact("title"));
        }
  
        return redirect('/')->with('error', 'You are not allowed to access');
    }
    public function signOut() {
        Session::flush();
        Auth::logout();
  
        return redirect('/login')->with('error', 'You have logged out from system');
    }

}
