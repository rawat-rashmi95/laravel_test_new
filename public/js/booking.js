  $(function () {
  	$('#booking-tbl').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    $('#no_of_tickets').keyup(function(){
    	getPrice();
    });
    function getPrice(){
    	var flight_type = $('#flight_type').val();
    	var book_time = $('#book_time').val();
    	var no_of_tickets = $('#no_of_tickets').val();
    	var users = $('#users').val();
    	if(flight_type != '' && book_time != '' && no_of_tickets != ''){
    		 

    		 var formData = {
    		 	flight_type: flight_type,
	            book_time: book_time,
	            no_of_tickets: no_of_tickets,
	            users: users,
	        };
	        $.ajaxSetup({
			    beforeSend: function(xhr, type) {
			    	$('.ajax-loader').css("display", "block");
			        if (!type.crossDomain) {
			            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
			        }
			    },
			});
    		$.ajax({
               type:'POST',
               url:'/booking/get_price',
               data:formData,
               success:function(data) {
                   $("#total_price").val(data.price);
                   $("#price-txt").text(data.price);
                   $('.ajax-loader').css("display", "none");
               }
               
            });
    	}
    }
  });