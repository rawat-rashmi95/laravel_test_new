  $(function () {
    $('#users-tbl').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#user_birth_date').datetimepicker({
        format: 'L'
    });
    
  });