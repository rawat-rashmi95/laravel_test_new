@extends('theme.logindefault')

@section('content')
<div class="container margin-tp">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          @if(session()->has('error'))
            <div class="col-lg-12 col-6">
              <div class="alert alert-danger">
                  {{ session()->get('error') }}
              </div>
            </div>
          @endif
           <div class="col-lg-12 col-6">
            <!-- small box -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Application Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" action="{{ route('application.save') }}" method="POST">
                @csrf
                <div class="card-body">
                  <h1>Basic Details:</h1>
                  <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="name" placeholder="ex: (Max John)" name="name">
                           @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email">
                           @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                      <textarea name="address" rows="5" cols="120">
                        
                      </textarea>
                           @if ($errors->has('address'))
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Gender</label>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="gender" id="gender1">
                      <label class="form-check-label" for="gender1">
                        Male
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="gender" id="gender2" checked>
                      <label class="form-check-label" for="gender2">
                        Female
                      </label>
                    </div>
                          @if ($errors->has('gender'))
                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                          @endif
                  </div>
                  <div class="form-group row">
                    <label for="contact_number" class="col-sm-2 col-form-label">Contact Number</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="contact_number" placeholder="Contact Number" name="contact_number">
                           @if ($errors->has('contact_number'))
                                <span class="text-danger">{{ $errors->first('contact_number') }}</span>
                            @endif
                    </div>
                  </div>
                  <h1>Educational Details:</h1>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">SSC</label>
                    <div class="col-sm-2">
                      <input type="text" name="ssc_field" value="SSC Board" readonly="readonly" class="form-control">
                    </div>
                    <label for="" class="col-sm-1 col-form-label">Year</label>
                    <div class="col-sm-2">
                      <input type="text" name="ssc_year" value="" class="form-control">
                    </div>
                    <label for="" class="col-sm-2 col-form-label">CGPA/PER</label>
                    <div class="col-sm-2">
                      <input type="text" name="ssc_marks" value="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">HSC</label>
                    <div class="col-sm-2">
                      <input type="text" name="hsc_field" value="HSC Board" readonly="readonly" class="form-control">
                    </div>
                    <label for="" class="col-sm-1 col-form-label">Year</label>
                    <div class="col-sm-2">
                      <input type="text" name="hsc_year" value="" class="form-control">
                    </div>
                    <label for="" class="col-sm-2 col-form-label">CGPA/PER</label>
                    <div class="col-sm-2">
                      <input type="text" name="hsc_marks" value="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Graduation</label>
                    <div class="col-sm-2">
                      <input type="text" name="graduation_field" value="Graduation" readonly="readonly" class="form-control">
                    </div>
                    <label for="" class="col-sm-1 col-form-label">Year</label>
                    <div class="col-sm-2">
                      <input type="text" name="graduation_year" value="" class="form-control">
                    </div>
                    <label for="" class="col-sm-2 col-form-label">CGPA/PER</label>
                    <div class="col-sm-2">
                      <input type="text" name="graduation_marks" value="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Master Degree</label>
                    <div class="col-sm-2">
                      <input type="text" name="master_degree_field" value="Master Degree" readonly="readonly" class="form-control">
                    </div>
                    <label for="" class="col-sm-1 col-form-label">Year</label>
                     <div class="col-sm-2">
                      <input type="text" name="master_degree_year" value="" class="form-control">
                    </div>
                    <label for="" class="col-sm-2 col-form-label">CGPA/PER</label>
                    <div class="col-sm-2">
                      <input type="text" name="master_marks" value="" class="form-control">
                    </div>
                  </div>
                  <h1>Work Experience:</h1>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
          </div>

          <!-- ./col -->
       
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
@endsection