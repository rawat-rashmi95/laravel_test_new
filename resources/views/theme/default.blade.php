<!DOCTYPE html>
<html lang="en">
@include('theme.head')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  @include('theme.header')

  @include('theme.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @yield('content')
  </div>
  <!-- /.content-wrapper -->

   @include('theme.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{!! asset('/plugins/jquery/jquery.min.js') !!}"></script>

<script src="{!! asset('/js/custom.js') !!}"></script>
<!-- Bootstrap 4 -->
<script src="{!! asset('/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
<!-- Select2 -->
<script src="{!! asset('/plugins/select2/js/select2.full.min.js') !!}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{!! asset('/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') !!}"></script>
<!-- InputMask -->
<script src="{!! asset('/plugins/moment/moment.min.js') !!}"></script>
<script src="{!! asset('/plugins/inputmask/jquery.inputmask.min.js') !!}"></script>
<!-- date-range-picker -->
<script src="{!! asset('/plugins/daterangepicker/daterangepicker.js') !!}"></script>

<!-- DataTables  & Plugins -->
<script src="{!! asset('/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! asset('/plugins/jszip/jszip.min.js') !!}"></script>
<script src="{!! asset('/plugins/pdfmake/pdfmake.min.js') !!}"></script>
<script src="{!! asset('/plugins/pdfmake/vfs_fonts.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-buttons/js/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-buttons/js/buttons.print.min.js') !!}"></script>
<script src="{!! asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') !!}"></script>
<!-- bootstrap color picker -->
<script src="{!! asset('/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') !!}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{!! asset('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
<!-- Bootstrap Switch -->
<script src="{!! asset('/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}"></script>
<!-- BS-Stepper -->
<script src="{!! asset('/plugins/bs-stepper/js/bs-stepper.min.js') !!}"></script>
<!-- dropzonejs -->
<script src="{!! asset('/plugins/dropzone/min/dropzone.min.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('/dist/js/adminlte.min.js') !!}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{!! asset('/dist/js/demo.js') !!}"></script>
<!-- Page specific script -->
<script src="{!! asset('/js/booking.js') !!}"></script>
<script src="{!! asset('/js/user.js') !!}"></script>

</body>
</html>
